export function getObjectType(obj: any) {
    return Object.prototype.toString.call(obj).slice(8, -1);
}

export function isComponentWillChange(oldValue: any, newValue: any) {
    const oldType = getObjectType(oldValue);
    const newType = getObjectType(newValue);
    return newType !== oldType
}