const CHANGE_DATA = "CHANGE_DATA";
const DELETE_NODE = "DELETE_NODE";
const ADD_NODE = "ADD_NODE";
const INSERT_ARRAY = "INSERT_ARRAY";
const CHANGE_KEY = "CHANGE_KEY";

export default {
  CHANGE_DATA,
  DELETE_NODE,
  ADD_NODE,
  INSERT_ARRAY,
  CHANGE_KEY
};
