import types from "./types";
import objectPath from "object-path";
import { isArray } from "util";

const INITIAL_STATE = {
  data: {}
};

const taskReducer = (state = INITIAL_STATE, action) => {
  const data = state.data;
  const { key, value, path, position } = action;

  switch (action.type) {
    case types.CHANGE_DATA:
      if (Object.keys(data).length === 0) {
        objectPath.set(data, "data", value);
      } else {
        objectPath.set(data, key, value);
      }
      return { ...state, ...data };
    case types.DELETE_NODE:
      objectPath.del(data, key);

      return { ...state, ...data };
    case types.ADD_NODE:
      if (path.length === 0) {
        objectPath.set(data, key, value);
      } else {
        objectPath.set(data, `${path}.${key}`, value);
      }
      return { ...state, ...data };
    case types.INSERT_ARRAY:
      objectPath.insert(data, `${path}`, value, position);
      return { ...state, ...data };
    case types.CHANGE_KEY:
      const copyDataforPath = objectPath.get(data, `${path}`);
      console.log(typeof objectPath.get(data, `${path}.${value}`))
      objectPath.del(data, `${path}`);
      if (key === path) {
        objectPath.set(
          data,
          `${value}`,
          copyDataforPath
        );
      } else {
        objectPath.set(
          data,
          `${path.slice(0, path.length - (key.length + 1))}.${value}`,
          copyDataforPath
        );
      }
      return { ...state, ...data };
    default:
      return state;
  }
};

export default taskReducer;
