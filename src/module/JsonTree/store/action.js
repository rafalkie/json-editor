import { getService } from "./services";

export function changeValue(key,value) {
  return {
    type: "CHANGE_DATA",
    value,
    key
  };
}
export function changeKey(path,key,value) {
  return {
    type: "CHANGE_KEY",
    value,
    key,
    path
  };
}
export function deleteValue(key) {
  return {
    type: "DELETE_NODE",
    key
  };
}

export function addValue(path,value,key) {
  return {
    type: "ADD_NODE",
    path,
    value,
    key
  };
}
export function insertValue(path,value,key,position) {
  return {
    type: "INSERT_ARRAY",
    path,
    value,
    key,
    position
  };
}
