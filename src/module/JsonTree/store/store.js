
import { createStore, compose } from 'redux';
import taskReducer from './reducer';
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ &&

window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({ 
trace: true, 
traceLimit: 25 
}) || compose; 
// const composeEnhancer = compose;
export const store = createStore(
  taskReducer,composeEnhancer()
);
