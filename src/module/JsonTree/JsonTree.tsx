import React, { useState, useEffect } from "react";
import { getObjectType } from "./helpers/objectTypes";
import JsonNode from "./component/JsonNode";
import "./style.scss";
import { changeValue } from "./store/action";
import { useSelector, useDispatch } from "react-redux";

interface Props {
  data: any;
  setData: (data:any) => void;
}
function JsonTree(props: Props) {
  const state = useSelector((state: any) => state);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(changeValue("", props.data));
  }, []);

  useEffect(() => {
    props.setData(state.data);
  }, [state.data]);

  const dataType = getObjectType(props.data);
  let node = null;

  if (dataType === "Object" || dataType === "Array") {
    node = <JsonNode data={state.data} name="" deep={-1} />;
  } else {
    node = "Dane muszą być obiektem lub tablicą";
  }

  return <> {node}</>;
}

export default JsonTree;
