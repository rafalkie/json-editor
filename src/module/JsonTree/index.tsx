import React from "react";
import "./style.scss";
import { store } from "./store/store";
import { Provider } from "react-redux";
import JsonTree from "./JsonTree";

interface Props {
  data: any;
  setData: (data: any) => void;
}
function Tree(props: Props) {
    const {setData,data} = props;
  return (
    <Provider store={store}>
      <JsonTree setData={setData} data={data} />
    </Provider>
  );
}

export default Tree;
