import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { changeValue, deleteValue, changeKey } from "../store/action";
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";

interface Props {
  value: string | number | null | undefined;
  name: string;
  keyPath: Array<string>;
  deep: number;
}
function JsonValue(props: Props) {
  const dispatch = useDispatch();
  const { name, keyPath } = props;

  const [edit, setEdit] = useState(false);
  const [value, setValue] = useState(props.value);
  const [newKey, setNewKey] = useState(name);
  const [newKeyVisible, setNewKeyVisible] = useState(false);
  const path =
    keyPath.length === 0
      ? name
      : `${keyPath.toString().replace(/,/g, ".")}.${name}`;
  const typeInput = typeof props.value;

  function save() {
    if (typeInput === "number") {
      dispatch(changeValue(path, parseInt(`${value}`)));
    } else {
      dispatch(changeValue(path, value));
    }
    setEdit(false);
  }
  function cancel() {
    setValue(props.value);
    setEdit(false);
  }
  const saveNewKey = () => {
    dispatch(changeKey(path, name, newKey));
    setNewKeyVisible(false);
  };
  return (
    <li>
      {newKeyVisible ? (
        <>
          <input
            value={newKey}
            placeholder="klucz"
            onChange={e => setNewKey(e.target.value)}
          />
          <button onClick={saveNewKey}>Zapisz</button>
          <button onClick={() => setNewKeyVisible(false)}>Anuluj</button>
        </>
      ) : (
        <ContextMenuTrigger id={`menu__value__${path}__${name}`}>
          <span className="tree__value"> {name}</span>{" "}
        </ContextMenuTrigger>
      )}
      {" : "}
      {edit ? (
        <>
          <input
            type={typeInput}
            value={value || ""}
            onChange={e => setValue(e.target.value)}
          />
          <button onClick={save}>Zapisz</button>
          <button onClick={cancel}>Anuluj</button>
        </>
      ) : (
        <>
          <span>{value}</span>
        </>
      )}
      <ContextMenu id={`menu__value__${path}__${name}`}>
        <MenuItem onClick={() => setEdit(true)}>Edytuj wartość</MenuItem>
        <MenuItem onClick={() => setNewKeyVisible(true)}>Edytuj klucz</MenuItem>
        <MenuItem onClick={() => dispatch(deleteValue(path))}>Usuń</MenuItem>
      </ContextMenu>
    </li>
  );
}

export default JsonValue;
