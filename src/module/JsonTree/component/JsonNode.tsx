import React, { useState } from 'react'
import dataTypes from '../helpers/dataTypes';
import { getObjectType } from '../helpers/objectTypes';
import JsonObject from './JsonObject';
import JsonArray from './JsonArray';
import JsonValue from './JsonValue';
var objectPath = require("object-path");

interface Props {
    data: any,
    name: string,
    keyPath?: any[]
    deep: number,
}
function JsonNode(props: Props) {
    const { data, name, deep } = props;
    const [keyPath, setKeyPath] = useState(props.keyPath || []);
    const dataType = getObjectType(data);


    switch (dataType) {
        case dataTypes.ERROR:
            return (<JsonObject
                data={data}
                name={name}
                keyPath={keyPath}
                deep={deep}
            />);
        case dataTypes.OBJECT:
            return (<JsonObject
                data={data}
                name={name}
                keyPath={keyPath}
                deep={deep}

            />);
        case dataTypes.ARRAY:
            return (<JsonArray
                data={data}
                name={name}
                keyPath={keyPath}
                deep={deep}
            />);
        case dataTypes.STRING:
            return (<JsonValue
                value={data}
                name={name}
                keyPath={keyPath}
                deep={deep}
            />);
        case dataTypes.NUMBER:
            return (<JsonValue
                value={data}
                name={name}
                keyPath={keyPath}
                deep={deep}

            />);
        case dataTypes.BOOLEAN:
            return (<JsonValue
                value={data}
                name={name}
                keyPath={keyPath}
                deep={deep}

            />);
        case dataTypes.DATE:
            return (<JsonValue
                value={data}
                name={name}
                keyPath={keyPath}
                deep={deep}

            />);
        case dataTypes.NULL:
            return (<JsonValue
                value={data}
                name={name}
                keyPath={keyPath}
                deep={deep}

            />);
        case dataTypes.UNDEFINED:
            return (<JsonValue
                value={data}
                name={name}
                keyPath={keyPath}
                deep={deep}

            />);
        case dataTypes.SYMBOL:
            return (<JsonValue
                value={data}
                name={name}
                keyPath={keyPath}
                deep={deep}

            />);
        default:
            return null;
    }
}

export default JsonNode
