import React, { useState, useEffect, Fragment } from "react";
import JsonNode from "./JsonNode";
import { useDispatch } from "react-redux";
import { deleteValue, insertValue, changeKey } from "../store/action";
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";

interface Props {
  data: any;
  name: string;
  keyPath: Array<string>;
  deep: number;
}
function JsonArray(props: Props) {
  const { name, deep, data } = props;

  const [visibleAddNewNode, setVisibleAddNewNode] = useState(false);
  const [type, setType] = useState("string");
  const [value, setValue] = useState("");
  const [key, setKey] = useState("");
  const dispatch = useDispatch();
  const [newKey, setNewKey] = useState(name);
  const [newKeyVisible, setNewKeyVisible] = useState(false);
  const keyPath = deep === -1 ? [] : [...props.keyPath, name];

  const path =
    keyPath.length === 0 ? name : `${keyPath.toString().replace(/,/g, ".")}`;
  const list = data.map((item: any, key: number) => (
    <Fragment key={key}>
      <JsonNode
        key={item}
        data={item}
        name={`${key}`}
        keyPath={keyPath}
        deep={deep + 1}
      />
    </Fragment>
  ));
  const save = () => {
    let valueDefault = null;
    if (type === "string") {
      valueDefault = `${value}`;
    } else if (type === "number") {
      valueDefault = parseInt(value);
    } else if (type === "array") {
      valueDefault = [`${value}`];
    } else if (type === "object") {
      valueDefault = {};
    }
    dispatch(insertValue(path, valueDefault, key, data.length));
  };
  const saveNewKey = () => {
    dispatch(changeKey(path, name, newKey));
    setNewKeyVisible(false);
  };
  return (
    <div>
      {newKeyVisible ? (
              <>
                <input
                  value={newKey}
                  placeholder="klucz"
                  onChange={e => setNewKey(e.target.value)}
                />
                <button onClick={saveNewKey}>Zapisz</button>
                <button onClick={() => setNewKeyVisible(false)}>Anuluj</button>
              </>
            ) : (
              <ContextMenuTrigger id={`menu__array__${path}__${name}`}>
        <span className="tree__object tree__object--hover"> {name}</span>
        <span className="tree__object">{": ["}</span>
      </ContextMenuTrigger>
            )}

      <ul>
        {list}
        {visibleAddNewNode ? (
          <li>
            <input
              value={key}
              placeholder="klucz"
              onChange={e => setKey(e.target.value)}
            />

            <input
              type={type}
              value={value}
              placeholder="wartość"
              onChange={e => setValue(e.target.value)}
            />
            <select value={type} onChange={e => setType(e.target.value)}>
              <option value="string">String</option>
              <option value="number">Number</option>
              <option value="array">Array</option>
              <option value="object">Object</option>
            </select>
            <button onClick={save}>Zapisz</button>
            <button onClick={() => setVisibleAddNewNode(false)}>Anuluj</button>
            <br />
          </li>
        ) : null}
      </ul>

      <span className="tree__object">{"]"}</span>
      <ContextMenu id={`menu__array__${path}__${name}`}>
        <MenuItem onClick={() => setVisibleAddNewNode(true)}>
          Dodaj nowy element
        </MenuItem>
        <MenuItem onClick={() => setNewKeyVisible(true)}>Edytuj klucz</MenuItem>
        <MenuItem onClick={() => dispatch(deleteValue(path))}>Usuń</MenuItem>
      </ContextMenu>
    </div>
  );
}

export default JsonArray;
