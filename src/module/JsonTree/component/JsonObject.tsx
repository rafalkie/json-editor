import React, { useState, Fragment } from "react";
import JsonNode from "./JsonNode";
import { useDispatch } from "react-redux";
import { deleteValue, addValue, changeKey } from "../store/action";
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";

interface Props {
  data: any;
  name: string;
  keyPath: Array<string>;
  deep: number;
}
function JsonObject(props: Props) {
  const { data, name, deep } = props;

  const [visibleAddNewNode, setVisibleAddNewNode] = useState(false);
  const [type, setType] = useState("string");
  const [value, setValue] = useState("");
  const [key, setKey] = useState("");
  const [newKey, setNewKey] = useState(name);
  const [newKeyVisible, setNewKeyVisible] = useState(false);

  const dispatch = useDispatch();

  const keyPath = deep === -1 ? [] : [...props.keyPath, name];

  const keyList = Object.getOwnPropertyNames(data);
  const path =
    keyPath.length === 0 ? name : `${keyPath.toString().replace(/,/g, ".")}`;
  const list = keyList.map(key => (
    <Fragment key={key}>
      <JsonNode
        key={key}
        data={data[key]}
        name={key}
        keyPath={keyPath}
        deep={deep + 1}
      />
    </Fragment>
  ));
  const save = () => {
    let valueDefault = null;
    if (type === "string") {
      valueDefault = `${value}`;
    } else if (type === "number") {
      valueDefault = parseInt(value) || 0;
    } else if (type === "array") {
      valueDefault = [`${value}`];
    } else if (type === "object") {
      valueDefault = {};
    }
    dispatch(addValue(path, valueDefault, key));
  };
  const saveNewKey = () => {
    dispatch(changeKey(path, name, newKey));
    setNewKeyVisible(false);
  };
  return (
    <div>
      <ul>
        <li>
          <span className="tree__object tree__object--hover">
            {newKeyVisible ? (
              <>
                <input
                  value={newKey}
                  placeholder="klucz"
                  onChange={e => setNewKey(e.target.value)}
                />
                <button onClick={saveNewKey}>Zapisz</button>
                <button onClick={() => setNewKeyVisible(false)}>Anuluj</button>
              </>
            ) : (
              <ContextMenuTrigger id={`menu__object__${path}__${name}`}>
                <span onClick={() => setNewKeyVisible(true)}> {name}: </span>
              </ContextMenuTrigger>
            )}
          </span>
          <span className="tree__object">{" {"}</span>
        </li>
        {list}
        {visibleAddNewNode ? (
          <li>
            <input
              value={key}
              placeholder="klucz"
              onChange={e => setKey(e.target.value)}
            />

            <input
              type={type}
              value={value}
              placeholder="wartość"
              onChange={e => setValue(e.target.value)}
            />
            <select value={type} onChange={e => setType(e.target.value)}>
              <option value="string">String</option>
              <option value="number">Number</option>
              <option value="array">Array</option>
              <option value="object">Object</option>
            </select>
            <button onClick={save}>Zapisz</button>
            <button onClick={() => setVisibleAddNewNode(false)}>Anuluj</button>
            <br />
          </li>
        ) : null}
      </ul>

      <span className="tree__object">{"}"}</span>
      <ContextMenu id={`menu__object__${path}__${name}`}>
        <MenuItem onClick={() => setVisibleAddNewNode(true)}>
          Dodaj nowy element
        </MenuItem>
        <MenuItem onClick={() => setNewKeyVisible(true)}>Edytuj klucz</MenuItem>
        <MenuItem onClick={() => dispatch(deleteValue(path))}>Usuń</MenuItem>
      </ContextMenu>
    </div>
  );
}

export default JsonObject;
