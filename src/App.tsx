import React, { useState } from "react";
import "./App.css";
import Tree from "./module/JsonTree";

function App() {
  const [data, setData] = useState({
    "glossary": [{
        "title": "example glossary",
		"GlossDiv": {
            "title": "S",
			"GlossList": {
                "GlossEntry": {
                    "ID": "SGML",
					"SortAs": "SGML",
					"GlossTerm": "Standard Generalized Markup Language",
					"Acronym": "SGML",
					"Abbrev": "ISO 8879:1986",
					"GlossDef": {
                        "para": "A meta-markup language, used to create markup languages such as DocBook.",
						"GlossSeeAlso": ["GML", "XML"]
                    },
					"GlossSee": "markup"
                }
            }
        }
    }]
});
const mam = {
  "d":"dd",
  1:"Dd"
}
console.log(mam)
  return (
    <div>
      <Tree data={data} setData={setData} />
    </div>
  );
}

export default App;
